#include "processwatcher.h"
#include "utils/utils.h"
#include <QDir>

ProcessWatcher::ProcessWatcher(QObject* parent) : QThread(parent)
{

}

void ProcessWatcher::run()
{
    m_timer.setInterval(1000);
    connect(&m_timer, &QTimer::timeout, this, &ProcessWatcher::scan);
    m_timer.start();
    this->exec();
}

void ProcessWatcher::scan()
{
    bool flag = false;
    if(m_targetPath.isEmpty()) { return; }
    auto pslist = Utils::findProcessByPath(m_targetPath);

    if(!m_targetModule.isEmpty()) {
        for(auto& proc: pslist){
            bool res = Utils::isProcessHasModule(proc.pid, m_targetModule);
            proc.injected = res;
        }
    }

    if(pslist.size() != m_lastquery.size()) {
        flag = true;
    } else {
        auto a = pslist.begin();
        auto b = m_lastquery.begin();
        while(a != pslist.end() && b != m_lastquery.end()) {
            if(a->pid != b->pid || a->injected != b->injected) {
                flag = true;
                break;
            }
            a++;
            b++;
        }
    }

    if(flag) {
        m_lastquery = pslist;
        emit signalTargetStatusUpdated(pslist);
    }

}

void ProcessWatcher::setTargetPath(QString path)
{
    m_targetPath = QDir::toNativeSeparators(path);
}

void ProcessWatcher::setTargetModule(QString modpath)
{
    m_targetModule = QDir::toNativeSeparators(modpath);
}
