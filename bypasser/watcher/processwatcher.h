#ifndef PROCESSWATCHER_H
#define PROCESSWATCHER_H

#include <QObject>
#include <QThread>
#include <QTimer>
#include "utils/utils.h"

class ProcessWatcher : public QThread
{
    Q_OBJECT
public:
    ProcessWatcher(QObject *parent = nullptr);

    void run() override;

private:
    void scan();

public slots:
    void setTargetPath(QString);
    void setTargetModule(QString);

signals:
    void signalTargetStatusUpdated(QList<Utils::Process>);

private:
    QString m_targetPath;
    QString m_targetModule;
    QList<Utils::Process> m_lastquery;
    QString m_lastmsg;
    QTimer m_timer;
};

#endif // PROCESSWATCHER_H
