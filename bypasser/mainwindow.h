#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "injector/injectorrunner.h"
#include "watcher/processwatcher.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

public slots:
    void onInject(bool);
    void onSelectGameExecutable(bool);
    void onSelectLauncherExecutable(bool);

    void refreshLauncherWatcher(QString);
    void refreshGameWatcher(QString);

    void onGameStatusUpdated(QList<Utils::Process> pslist);
    void onLauncherStatusUpdated(QList<Utils::Process> pslist);

private:
    Ui::MainWindow *ui;
    QString m_launcherPath;
    QList<Utils::Process> m_launcherProcs;
    bool m_launcherRunning;
    QString m_gamePath;
    bool m_gameRunning;
    InjectorRunner* m_injector;
    ProcessWatcher* m_launcherwatcher;
    ProcessWatcher* m_gamewatcher;
};
#endif // MAINWINDOW_H
