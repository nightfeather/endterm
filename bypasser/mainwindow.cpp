#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QFileDialog>
#include <QMessageBox>

const static QString stage1_payload = QDir::toNativeSeparators(QFileInfo(".\\stage1.dll").absoluteFilePath());
const static QString stage2_payload = QDir::toNativeSeparators(QFileInfo(".\\stage2.dll").absoluteFilePath());

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
    , m_launcherRunning(false)
    , m_gameRunning(false)
    , m_injector(nullptr)
    , m_launcherwatcher(new ProcessWatcher(this))
    , m_gamewatcher(new ProcessWatcher(this))
{
    ui->setupUi(this);

    m_gamePath.clear();
    m_launcherPath.clear();

    connect(ui->injectBtn, &QPushButton::clicked, this, &MainWindow::onInject);
    connect(ui->launcherSelectorBtn, &QToolButton::clicked, this, &MainWindow::onSelectLauncherExecutable);
    connect(ui->gameSelectorBtn, &QToolButton::clicked, this, &MainWindow::onSelectGameExecutable);

    connect(m_launcherwatcher, &ProcessWatcher::signalTargetStatusUpdated,
            this, &MainWindow::onLauncherStatusUpdated);
    connect(m_gamewatcher, &ProcessWatcher::signalTargetStatusUpdated,
            this, &MainWindow::onGameStatusUpdated);
}

MainWindow::~MainWindow()
{
    delete ui;
    this->disconnect();
    m_launcherwatcher->quit();
    m_gamewatcher->quit();
    if(m_injector) { m_injector->quit(); }
}

void MainWindow::onInject(bool)
{
    if(m_launcherPath.isEmpty()) {
        QMessageBox::warning(this, "Warning", "Your ancestor didn't left the path to launcher field empty.");
        return;
    }

    if(!m_launcherRunning) {
        QMessageBox::warning(this, "Warning", "You should start the launcher before start injection,\nor you didn't run this with admin priviledge.");
        return;
    }

    if(m_gamePath.isEmpty()) {
        QMessageBox::warning(this, "Warning", "Your ancestor didn't left the path to game field empty.");
        return;
    }

    if(m_gameRunning) {
        QMessageBox::warning(this, "Warning", "You should stop the game before start injection.");
        return;
    }

    for(auto& proc: m_launcherProcs) {
        bool res = Utils::loadModuleInProcess(proc.pid, stage1_payload);
        if(!res) {
            qDebug() << "Cannot load dll into process";
        }
    }
}

void MainWindow::onSelectGameExecutable(bool)
{
    QFileDialog dialog(this);
    QStringList filters;
    filters << "Executable Files (*.exe)"
            << "Any Files (*)";
    dialog.setFileMode(QFileDialog::FileMode::ExistingFile);
    dialog.setNameFilters(filters);
    if(dialog.exec() == QDialog::Accepted) {
        refreshGameWatcher(dialog.selectedFiles()[0]);
    }
}

void MainWindow::onSelectLauncherExecutable(bool)
{
    QFileDialog dialog(this);
    QStringList filters;
    filters << "Executable Files (*.exe)"
            << "Any Files (*)";
    dialog.setFileMode(QFileDialog::FileMode::ExistingFile);
    dialog.setNameFilters(filters);
    if(dialog.exec() == QDialog::Accepted) {
        refreshLauncherWatcher(dialog.selectedFiles()[0]);
    }
}

void MainWindow::refreshLauncherWatcher(QString path)
{
    m_launcherPath = path;
    ui->launcherSelectorText->setText(m_launcherPath);
    m_launcherwatcher->setTargetPath(m_launcherPath);
    m_launcherwatcher->setTargetModule(stage1_payload);
    if(!m_launcherwatcher->isRunning()) {
        if(m_launcherwatcher->isFinished()) {
            m_launcherwatcher = new ProcessWatcher(this);
            connect(m_launcherwatcher, &ProcessWatcher::signalTargetStatusUpdated,
                    this, &MainWindow::onLauncherStatusUpdated);
        }
        m_launcherwatcher->run();
    }
}

void MainWindow::refreshGameWatcher(QString path)
{
    m_gamePath = path;
    ui->gameSelectorText->setText(m_gamePath);
    m_gamewatcher->setTargetModule(stage2_payload);
    if(!m_gamewatcher->isRunning()) {
        if(m_gamewatcher->isFinished()) {
            m_gamewatcher = new ProcessWatcher(this);
            connect(m_gamewatcher, &ProcessWatcher::signalTargetStatusUpdated,
                    this, &MainWindow::onGameStatusUpdated);
        }
        m_gamewatcher->run();
    }
    m_gamewatcher->setTargetPath(m_gamePath);
}

void MainWindow::onGameStatusUpdated(QList<Utils::Process> pslist)
{
    if(pslist.isEmpty()) {
        m_gameRunning = false;
        ui->gameStatusText->setText("Process Not Running");
    } else {
        QString temp = "Process(%1, %2)";
        QStringList segs;
        m_gameRunning = true;
        for(auto& proc: pslist) {
            segs.append(temp.arg(proc.pid).arg(proc.injected ? "yes" : "no"));
        }
        ui->gameStatusText->setText(segs.join(", "));
    }

    if(m_gameRunning) {
        ui->gameStatusText->setStyleSheet("QLabel { color: red; }");
    } else {
        ui->gameStatusText->setStyleSheet("QLabel { color: green; }");
    }
}

void MainWindow::onLauncherStatusUpdated(QList<Utils::Process> pslist)
{
    if(pslist.isEmpty()) {
        m_launcherRunning = false;
        ui->launcherStatusText->setText("Process Not Running");
    } else {
        QString temp = "Process(%1, %2)";
        QStringList segs;
        m_launcherRunning = true;
        for(auto& proc: pslist) {
            segs.append(temp.arg(proc.pid).arg(proc.injected ? "yes" : "no"));
        }
        ui->launcherStatusText->setText(segs.join(", "));
    }

    if(m_launcherRunning) {
        ui->launcherStatusText->setStyleSheet("QLabel { color: green; }");
    } else {
        ui->launcherStatusText->setStyleSheet("QLabel { color: red; }");
    }

    m_launcherProcs = pslist;
}

