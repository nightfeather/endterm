#include "utils.h"

#include <windows.h>
#include <psapi.h>
#include <tlhelp32.h>
#include <shellapi.h>

namespace Utils {
    QList<Process> enumAllProcess() {
        QList<Process> pslist;
        PROCESSENTRY32W entry;
        entry.dwSize = sizeof(PROCESSENTRY32W);

        HANDLE snapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);

        if (Process32FirstW(snapshot, &entry) == TRUE)
        {
            do {
                Process proc;
                WCHAR path[260] = {0};
                HANDLE handle = OpenProcess(PROCESS_QUERY_INFORMATION | PROCESS_VM_READ, FALSE, entry.th32ProcessID);
                GetModuleFileNameExW(handle, nullptr, path, 260);
                CloseHandle(handle);

                proc.path = QString::fromWCharArray(path);
                proc.pid = entry.th32ProcessID;
                proc.injected = false;
                pslist.append(proc);
            } while (Process32NextW(snapshot, &entry) == TRUE);
        }

        CloseHandle(snapshot);
        return pslist;
    }

    QList<Module> enumProcessModules(uint32_t pid) {
        QList<Module> mlist;
        MODULEENTRY32W entry;
        entry.dwSize = sizeof(MODULEENTRY32W);

        HANDLE snapshot = CreateToolhelp32Snapshot(TH32CS_SNAPMODULE, pid);

        if (Module32FirstW(snapshot, &entry) == TRUE) {
            do {
                Module mod;
                mod.pid = pid;
                mod.path = QString::fromWCharArray(entry.szExePath);
                mod.base = entry.modBaseAddr;
                mlist.append(mod);
            } while (Module32NextW(snapshot, &entry) == TRUE);
        }

        CloseHandle(snapshot);
        return mlist;
    }

    QList<Process> findProcessByPath(QString path)
    {
        QList<Process> psList;
        for(auto& val: enumAllProcess()) {
            if(val.path == path) {
                psList.append(val);
            }
        }

        std::sort(psList.begin(), psList.end(), [](Process& a, Process& b){ return a.pid > b.pid; });

        return psList;
    }

    bool isProcessHasModule(uint32_t pid, QString modpath)
    {
        QList<Module> modlist = enumProcessModules(pid);
        return std::any_of(modlist.begin(), modlist.end(), [&modpath](Module& mod){ return mod.path == modpath; });
    }

    bool loadModuleInProcess(uint32_t pid, QString modpath)
    {
        HANDLE target = OpenProcess(PROCESS_ALL_ACCESS, FALSE, pid);
        WCHAR payload_path[260] = {0};
        modpath.toWCharArray(payload_path);
        size_t written;

        HANDLE hMem = VirtualAllocEx(target, nullptr, wcslen(payload_path)* sizeof(WCHAR), MEM_COMMIT|MEM_RESERVE, PAGE_READWRITE);
        if(!hMem) { return false; }

        HMODULE k32base = GetModuleHandleA("kernel32.dll");
        FARPROC hProc = GetProcAddress(k32base, "LoadLibraryW");
        auto result = WriteProcessMemory(target, hMem, payload_path, wcslen(payload_path)*sizeof(WCHAR), &written);
        if(!result) { return false; }

        HANDLE hThread = CreateRemoteThread(target, nullptr, 0, reinterpret_cast<LPTHREAD_START_ROUTINE>(hProc), hMem, 0, nullptr);
        WaitForSingleObject(hThread, INFINITE);

        VirtualFreeEx(target, hMem, wcslen(payload_path)*sizeof(WCHAR), MEM_DECOMMIT|MEM_RELEASE);
        CloseHandle(hThread);
        CloseHandle(target);
        return true;
    }

}

QDebug operator<<(QDebug dbg, const Utils::Process &proc)
{
    return dbg << QString("Process([%1]%2)").arg(proc.pid).arg(proc.path);
}
