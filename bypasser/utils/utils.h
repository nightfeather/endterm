#ifndef UTILS_H
#define UTILS_H

#include <QDebug>
#include <QMetaType>
#include <QObject>

namespace Utils {
    struct Process {
        uint32_t pid;
        QString path;
        bool injected = false;
    };

    struct Module {
        uint32_t pid;
        QString path;
        uchar* base;
    };

    QList<Process> enumAllProcess();
    QList<Module> enumProcessModules(uint32_t);
    QList<Process> findProcessByPath(QString);
    bool isProcessHasModule(uint32_t, QString);

    bool loadModuleInProcess(uint32_t, QString);
}

Q_DECLARE_METATYPE(Utils::Process)
Q_DECLARE_METATYPE(Utils::Module)
Q_DECLARE_METATYPE(QList<Utils::Process>)
Q_DECLARE_METATYPE(QList<Utils::Module>)

QDebug operator<<(QDebug dbg, const Utils::Process& proc);

#endif // UTILS_H
