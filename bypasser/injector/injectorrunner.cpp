#include "injectorrunner.h"

InjectorRunner::InjectorRunner(Utils::Process target, QString modpath, QObject* parent)
    : QThread(parent)
    , m_target(target)
    , m_modpath(modpath)
{
}

void InjectorRunner::run()
{
    this->exec();
}
