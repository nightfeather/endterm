#ifndef INJECTORRUNNER_H
#define INJECTORRUNNER_H

#include <QObject>
#include <QThread>
#include "utils/utils.h"

class InjectorRunner : public QThread
{
    Q_OBJECT
public:
    InjectorRunner(Utils::Process target, QString modpath, QObject* parent=nullptr);
    ~InjectorRunner() override = default;

    void run() override;
private:
    Utils::Process m_target;
    QString m_modpath;
};

#endif // INJECTORRUNNER_H
